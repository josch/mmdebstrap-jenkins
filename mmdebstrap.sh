#!/bin/sh

set -exu

export DEFAULT_DIST=unstable
export RUN_MA_SAME_TESTS=no
export HAVE_UNSHARE=yes
export HAVE_QEMU=yes
export HAVE_PROOT=no
export HAVE_BINFMT=yes
export http_proxy="http://127.0.0.1:3128"

# in case there is a leftover process from the last run, kill it
pkill --full --exact "python3 ./caching_proxy.py ./shared/cache.[AB] ./shared/cache.[AB]" || :

DISK_SIZE=10G CMD=mmdebstrap ./make_mirror.sh
CMD=mmdebstrap ./coverage.sh
