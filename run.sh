#!/bin/sh

set -exu

# change working directory to where this script is located

cd "$(dirname "$(readlink -f "$0")")"

ls -lha /proc/sys/fs/binfmt_misc
ls -lha /sys/module/binfmt_misc
ls -lha /sys/module/kvm
ls -lha /dev/kvm
lsmod | grep kvm
lsmod | grep binfmt_misc
whoami
groups
/usr/sbin/update-binfmts --display
dpkg -l
df -h
mount
ip addr
nproc
uname -a

git -C mmdebstrap.git restore make_mirror.sh

sudo mmdebstrap --variant=apt --mode=root \
	--include=apt-cudf,arch-test,aspcud,binfmt-support,black,curl,debian-archive-keyring,debootstrap,diffoscope,dpkg-dev,fakechroot,fakeroot,gpg,grep-dctrl,libcap2-bin,libguestfs-tools,libperl-critic-perl,linux-image-amd64,mmdebstrap,mount,openssh-client,perl-doc,perltidy,python3,python3-apt,qemu-system-x86,qemu-user,qemu-user-static,sleepenh,squashfs-tools-ng,sudo,time,uidmap,python3-debian,ovmf,shellcheck,debvm,dosfstools,shfmt \
	--customize-hook='chroot "$1" mknod /dev/kvm c 10 232' \
	--customize-hook='chroot "$1" chown root:kvm /dev/kvm' \
	--customize-hook='for f in mmdebstrap.sh mmdebstrap.git/make_mirror.sh mmdebstrap.git/run_qemu.sh mmdebstrap.git/coverage.sh mmdebstrap.git/coverage.py mmdebstrap.git/coverage.txt mmdebstrap.git/tests mmdebstrap.git/hooks mmdebstrap.git/run_null.sh mmdebstrap.git/gpgvnoexpkeysig mmdebstrap.git/caching_proxy.py mmdebstrap.git/mmdebstrap-autopkgtest-build-qemu mmdebstrap.git/.perltidyrc; do cp -a "$f" "$1"; done' \
	--customize-hook='chroot "$1" sh mmdebstrap.sh' \
	unstable - > /dev/null
